#encoding: utf-8
from django.shortcuts import render, redirect
from .forms import MyForm

def myform(request):
	if request.method == "POST":
		form = MyForm(request.POST)
		if form.is_valid():
			# Validaremos aquí
			nombre = form.cleaned_data['nombre']
			print "IS_VALID", nombre
			return redirect("/")
	else:
		form = MyForm()

	return render(request, 'formulario/form.html', {"form": form})
