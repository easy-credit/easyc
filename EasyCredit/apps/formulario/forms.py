#encoding: utf-8
from django import forms


class MyForm(forms.Form):

	nombre = forms.CharField(label='Mi nombre', max_length=100, required=False)
	last_name= forms.CharField(label='last_name',max_length=100)
	email= forms.EmailField(label='email',max_length=50)
	

	def clean_nombre(self):

		if (self.cleaned_data.get("nombre", "").endswith("0000")):
			raise forms.ValidationError("Nombre inválida, prueba nuevamente")

		return self.cleaned_data.get("nombre")