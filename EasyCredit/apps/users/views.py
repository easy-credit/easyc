from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.views.generic import TemplateView  


def LogOut(request):
	logout(request)
	return redirect('/')	