# -*- coding: utf-8 -*-
from unipath import Path
BASE_DIR = Path(__file__).ancestor(3)


SECRET_KEY = 'p+1w1nqlf^&y_m=mk(&j)y*^)kun^$uunv2d%so7m1)+nm=xx9'

#aqui van todas las apps que voy a utilizar en el proyecto
DJANGO_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
)


#aqui van todas las apps que voy a utilizar en el proyecto
THIRD_PARTY_APPS=(
        #'south',
        'social.apps.django_app.default',
        'djrill',
)

#aqui van todas las apps que voy a utilizar en el proyecto
LOCAL_APPS=(
        #'SistemaDiscusiones.apps.home',
        'apps.home',
        'apps.users',
        'apps.formulario',

)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'EasyCredit.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR.child('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'EasyCredit.wsgi.application'


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


#Especificar el modelo de User
AUTH_USER_MODEL = 'users.User'


AUTHENTICATION_BACKENDS = (
 'social.backends.facebook.FacebookAppOAuth2',
 'social.backends.facebook.FacebookOAuth2',
 #'social.backends.twitter.TwitterOAuth',
 'django.contrib.auth.backends.ModelBackend',
 )

#Agrego el namespace a utilizar
#SOCIAL_AUTH_URL_NAMESPACE = 'social'
 
#Indico a que url va ir en caso de login exitoso 
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
 
#URL de error al loguearse en redes sociales
#SOCIAL_AUTH_LOGIN_URL = "/url_de_logueo/"
#SOCIAL_AUTH_LOGIN_URL = '/error/'
 
#Seteo el modelo de usuario 
SOCIAL_AUTH_USER_MODEL = 'users.User'
 
#Para obtener el email del usuario logueado ya que facebook no lclear
#o hace automático si twitter
SOCIAL_AUTH_FACEBOOK_SCOPE =['email']

SOCIAL_AUTH_PIPELINE = (
#Obtiene las instancias de social_user y user
'social.pipeline.social_auth.social_details',
'social.pipeline.social_auth.social_uid',
'social.pipeline.social_auth.auth_allowed',
#Recibe según el user.email la instancia del usuario y lo reemplaza con uno que recibió anteriormente
'social.pipeline.social_auth.social_user',
'social.pipeline.social_auth.associate_by_email',
#Intenta crear un username válido 
'social.pipeline.user.get_username',
#Crea un nuevo usuario si todavía no existe
'social.pipeline.user.create_user',
#Trata de asociar las cuentas
'social.pipeline.social_auth.associate_user',
#Recibe y actualiza social_user.extra_data
'social.pipeline.social_auth.load_extra_data',
#Actualiza los campos de la instancia user con la información que obtiene vía backend
#'social.pipeline.user.user_details',


'apps.users.pipelines.user_details',
'apps.users.pipelines.get_avatar',
'apps.users.pipelines.get_gender',
)

"""
Especificamos donde van a estar todos los archivos que voy a ocupar en el proyecto
BASE_DIR, directorio base de mi proyecto.

En este caso los archivos van a estar en la carpeta templates
Con la version de Django 1.8
esta variable la colocamos dentro de TEMPLATES

TEMPLATE_DIRS = [BASE_DIR.child('templates')]
"""

EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
